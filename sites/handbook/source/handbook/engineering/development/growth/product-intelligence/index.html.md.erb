---
layout: handbook-page-toc
title: Product Intelligence Group
description: "The Product Intelligence group work on feature enhancements and implementing privacy focused product analytics across GitLab projects"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Vision

The Product Intelligence Group is part of the [Growth section](/handbook/engineering/development/growth/). Our group focuses on providing GitLab's team with data-driven product insights to build a better GitLab. To do this, we build data collection and analytics tools within the GitLab product in a privacy-focused manner. Insights generated from Product Intelligence enables us to identify the best places to invest people and resources, what product categories mature faster, where our user experience can be improved, and how product changes impact the business. You can learn more about what we're building next on the [Product Intelligence Direction page](/direction/product-intelligence/).


How we work:
- We work in accordance with our [GitLab values](/handbook/values/).
- We work [transparently](/handbook/values/#transparency) with nearly everything public.
- We get a chance to work on the things we want to work on.
- We have a [bias for action](/handbook/values/#bias-for-action).
- We make data-driven decisions.
- Everyone can contribute to our work.

**I have a question. Who do I ask?**
Questions should start by @ mentioning the product manager for the [Product Intelligence Group](/handbook/product/categories/#product-intelligence-group) or by creating an issue in our [issue board](/handbook/engineering/development/growth/product-intelligence/#issue-boards).

## Team members

The following people are permanent members of the Product Intelligence Group:

<%= direct_team(manager_role: 'Fullstack Engineering Manager, Product Intelligence. Strategy and Operations Lead, China (Interim)') %>

## Project management process

Our team uses a hybrid of Scrum for our project management process. This process follows GitLab's [monthly milestone release cycle](/handbook/marketing/blog/release-posts/#monthly-releases).

- We only work off of issue boards which act as our single source of truth.
- We continuously progress issues to the next workflow stage.
- We work on both product and engineering initiatives.
- We prioritize and estimate all issues we work on.
- We do weekly refinement to ensure our issue board is always kept up to date.
- We do monthly milestone planning to prepare for our upcoming milestone.
- We do monthly roadmap planning to prepare for our next 6 milestones.

### Workflow

Our team use the following workflow stages defined in the [Product Development Flow](/handbook/product-development-flow/#workflow-summary):

#### Validation stage

| Label | Usage |
| -- | -- |
| `~"workflow::validation backlog"` | Applied by the Product Manager for incoming issues that have not been refined or prioritized. |
| `~"workflow::problem validation"` | Applied by the Product Manager for issues where the PM is developing a thorough understanding of the problem |
| `~"workflow::design"` | Applied by the Product Manager or Designer (or Product Intelligence Engineer) to ideate and propose solutions. The proposed solutions should be reviewed by engineering to ensure technical feasibility. |
| `~"workflow::solution validation"` | Applied by the Product Manager or Designer (or Product Intelligence Engineer) to validate a proposed solution through user interviews or usability testing. |

#### Build stage

| Label | Usage |
| -- | -- | -- |
| `~"workflow::planning breakdown"` | Applied by the Product Manager for Engineers to begin breaking down issues and adding estimates. |
| `~"workflow::scheduling"` | Applied by the Engineer while an issue is awaiting milestone assignment. |
| `~"workflow::ready for development"` |  Applied by the Engineer after an issue has been broken down and scheduled for development. |
| `~"workflow::in dev"` | Applied by the Engineer after work (including documentation) has begun on the issue. An MR is typically linked to the issue at this point. |
| `~"workflow::in review"` | Applied by the Engineer indicating that all MRs required to close an issue are in review. |
| `~"workflow::verification"` | Applied by the Engineer after the MRs in the issue have been merged, this label is applied signaling the issue needs to be verified in staging or production. |
| `~"workflow::blocked"` | Applied by any team member if at any time during development the issue is blocked. For example: technical issue, open question to PM or PD, cross-group dependency. |


### Epic roadmap

We use an epic roadmap to track epic progress on a quarterly basis. The epic roadmap is a live view of the [Product Intelligence Direction page](/direction/product-intelligence/).

To keep things simple, we primarily use the [gitlab.com/gitlab-org](https://gitlab.com/gitlab-org/) group for our roadmap. If epics are created on the [gitlab.com/gitlab-com](https://gitlab.com/gitlab-com/) and [gitlab.com/gitlab-services](https://gitlab.com/gitlab-services/) groups, we create placeholders of them on [gitlab.com/gitlab-org](https://gitlab.com/gitlab-org/) so that all epics show up in a single roadmap view.

| gitlab-org | gitlab-com | gitlab-services | all groups |
| ------ | ------ | ------ | ------ |
| [gitlab-org Epic Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?state=opened&sort=start_date_asc&label_name%5B%5D=group%3A%3Aproduct+intelligence) | [-](https://gitlab.com/groups/gitlab-com/-/roadmap?state=opened&sort=start_date_asc&label_name%5B%5D=group%3A%3Aproduct+intelligence) | [-](https://gitlab.com/groups/gitlab-services/-/roadmap?state=opened&sort=start_date_asc&label_name%5B%5D=group%3A%3Aproduct+intelligence) | |

### Issue boards

We use issue boards to track issue progress on a daily basis. Issue boards are our single source of truth for the status of our work. Issue boards should be viewed at the highest group level for visibility into all nested projects in a group.

There are three groups we use:
- The [gitlab.com/gitlab-org](https://gitlab.com/gitlab-org/) group includes the [gitlab](https://gitlab.com/gitlab-org/gitlab), [customers-gitlab-com](https://gitlab.com/gitlab-org/customers-gitlab-com), and [license-gitlab-com](https://gitlab.com/gitlab-org/license-gitlab-com) projects.
- The [gitlab.com/gitlab-com](https://gitlab.com/gitlab-com/) group includes the [www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com) project.
- The [gitlab.com/gitlab-services](https://gitlab.com/gitlab-services/) group includes the [version-gitlab-com](https://gitlab.com/gitlab-services/version-gitlab-com) project.

| gitlab-org | gitlab-com | gitlab-services | all groups |
| ------ | ------ | ------ | ------ |
| [gitlab-org Issue Board](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aproduct%20intelligence) | [gitlab-com Issue Board](https://gitlab.com/groups/gitlab-com/-/boards/1546862?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aproduct%20intelligence) | [gitlab-services Issue Board](https://gitlab.com/groups/gitlab-services/-/boards/1546865?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aproduct%20intelligence) | [-](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Agrowth&label_name[]=group%3A%3Aproduct%20intelligence) |

### Initiatives

The work done by our team mainly fall into two categories: product initiatives and engineering initiatives.

**Product initiatives:** This work is primarily related to driving value for our customers. This work is defined by a product manager and is outlined in the team's product roadmap.

**Engineering initiatives:** This work is primarily related to driving value for internal teams. This work is defined by an engineer and can include bug fixes, follow-up issues, refactoring, career development work, or anything an engineer thinks is important enough to be worked on.

### Prioritization

We prioritize our product roadmap using milestone priority labels:

- `~"milestone::p1"`
- `~"milestone::p2"`
- `~"milestone::p3"`
- `~"milestone::p4"`

Prioritization of our product roadmap is determined by our product managers. Every epic and issue that is part of a product roadmap should have a priority label.

We work from the highest to the lowest priority when working on product initiatives. For design prioritization, see [priority for UX issues](/handbook/engineering/ux/ux-designer/#priority-for-ux-issues).

### Iteration

We follow the [iteration process](/handbook/engineering/#iteration) outlined by the Engineering function.

### Estimation

We follow the [estimation process](/handbook/engineering/development/growth/#estimation) outlined by the Growth sub-department.

### Due dates

To properly set expectations for product managers and other stakeholders, our team may decide to add a due date onto an issue. Due dates are not meant to pressure our team but are instead used to communicate an expected delivery date.

We may also use due dates as a way to timebox our iterations. Instead of spending a month on shipping a feature, we may set a due date of a week to force ourselves to come up with a smaller iteration.

### Weekly refinement

Refinement is the responsibility of every team member. Every Friday, Slack will post a refinement reminder in our group channel. During refinement, we make sure that every issue on the issue board is kept up to date with the necessary details and next steps.

### Timeline

Our team pays close attention to the [Product Development Timeline](/handbook/engineering/workflow/#product-development-timeline) as our group is dependent on the [GitLab self-managed release cycle](https://about.gitlab.com/upcoming-releases/):

- Milestone planning issue is created on the `Previous Month 4th`.
- Milestone commitment is finalized on the `Previous Month 13th`.
- Milestones start on the `Previous Month 18th`.
- Milestones end on the `Current Month 17th`.
- [Team Retrospectives](/engineering/management/team-retrospectives/) start on the `Current Month 19th`.
- Release Day happens on the `Current Month 22nd`.
- [Retrospective Summary](/engineering/workflow/#retrospective) happen on the `Next Month 4th`.
- [Retrospective Discussion](/engineering/workflow/#retrospective) happen on the `Next Month 6th`.

In addition to these dates, our team also does the following:

- Team Retrospective discussion takes place as a part of our Product Intelligence Sync meetings and it occurs on the last meeting of every month.
- Monthly Roadmap Planning discussion takes place as a part of our Product Intelligence Sync meetings and it occurs on the first meeting of every month.

### Milestone Planning

Planning a milestone is a collaborative effort between Product, Engineering, and UX.

#### Milestone Planning Issue

We utilize a milestone planning issue to keep our team organized during the planning phase. Here is an [example milestone planning issue](https://gitlab.com/gitlab-org/gitlab/-/issues/330863). A milestone planning issue consists of:
- Links
- Holidays
- Validation Track
  - UX Capacity
  - Priorities
- Build Track
  - Engineering Capacity
  - Priorities
- Planning Tasks

#### Planning Tasks

The following tasks need to be completed for planning a milestone:
* [ ] PM creates milestone planning issue.
* [ ] Team updates `Holidays` section.
* [ ] UX updates `Validation Track - UX Capacity` section.
* [ ] Engineering updates `Build Track - Engineering Capacity` section.
* [ ] Team uses comments to suggest work for prioritization.
* [ ] PM reviews suggestions and updates `Validation Track - Priorities` and `Build Track - Priorities`.
* [ ] PM moves all work into ~"workflow::planning breakdown".
* [ ] PM assigns priority by using the scoped ~"milestone::p1" - ~"milestone::p4" labels.
* [ ] Engineers break down issues in ~"workflow::planning breakdown".
* [ ] Engineers estimate issues in ~"workflow::planning breakdown" then move to ~"workflow::scheduling".
* [ ] Engineers create milestone commitment by tagging issues in ~"workflow::scheduling" with the current milestone until milestone capacity is reached and milestone commitment is made. Issues tagged with the current milestone should also have the ~"Deliverable" label applied .
* [ ] Engineers move issues into ~"workflow::ready for development".
* [ ] Engineers begin working on issues in ~"workflow::ready for development".

#### Milestone Capacity

Our milestone capacity tells us how many issue weights we can expect to complete in a given milestone. This is calculated by taking the sum of issue weights completed in the last milestone prorated by holidays. If there is a large variation in the estimated capacity of the last milestone and the one before it, we will use an average estimated capacity of the last few milestones. Here is an example of how we calculate capacity:

**Last Milestone:**
* **Total weights completed:** 24
* **Available work days:** 21.6 * 1 engineers = 21.6 available days
* **Actual work days:** 21.6 available days - 5 days off = 16.6 actual days
* **Ratio of available:actual work days:** 21.6 / 16.6 = 1.30
* **Maximum capacity:** 24 * 1.3 = 31 weights

**Current Milestone:**
* **Available work days:** 21.6 days * 1 engineers = 21.6 available days
* **Actual work days:** 21.6 av-days - 0 days off = 21.6 actual days
* **Ratio of available:actual work days:** 21.6 / 21.6 = 1
* **Maximum capacity:** 31 * 1 = 31 weights

In this example, the current milestone capacity is 31 weights.

#### Milestone Commitment

A milestone commitment is a list of issues our team aims to complete in the milestone. After issues are broken down, estimated, and prioritized, we begin tagging issues with the current milestone until the sum of the tagged issue weights equals our team's milestone capacity. All issues in a milestone commitment should also have the `~Deliverable` label.

## Epics and issues

To help our team be [efficient](/handbook/values/#efficiency), we explicitly define how our team uses epics and issues.

### Epic and issue creation

We aim to create issues in the same project as where the future merge request will live. And we aim to create epics at the topmost-level group that makes the most sense for its collection of child epics and issues. For example, if an experiment is being run in the CustomersDot, the epic should be created in the `gitlab-org` group, and the issue should be created in the `gitlab-org/customers-gitlab-com` project.

We emphasize creating the epic at the topmost-level group so that it will show up on our epic roadmap. And we emphasize creating the issue in the right project to avoid having to close and move it later in the development process. If the location of the future merge request cannot be determined, we will create the issue in our catch-all [growth team-tasks project](https://gitlab.com/gitlab-org/growth/team-tasks/issues).

### Ratio of issues to MRs

We used to aim for a 1:1 ratio between issues and merge requests, mainly for the sake of status visibility at the issue board level. We have since moved to using epics and the epic roadmap for product management visibility, and we are comfortable with the amount of status updates received during our weekly sync meetings as well as through comments within issues themselves.

If an issue requires multiple merge requests, we no longer recommend splitting the issue itself up in order to maintain a 1:1 ratio of issues to MRs. The advantage is that an engineer is able to create an arbitrary number of MRs for a single issue and can move much more quickly through them. The trade-off is that doing so makes it more difficult to communicate the overall status of the issue itself. It is the engineer's responsibility to make sure that the status of each issue they are working on is effectively communicated to their Product Manager.

### Epics

We group related issues together using parent [epics](https://docs.gitlab.com/ee/user/group/epics/) and child epics, providing us with a better visual overview of our roadmap.

- The description of the parent epic should always be kept up-to-date as the single source of truth.
- The conversation about the implementation or design is done in issues.
- Designs in progress will be in UX issues, using the design tab for conversation, review, feedback, exploration, etc.
- The final designs should be linked in the parent epic description.
- Epics and Child Epics must have the same section and group labels to see them on our roadmap.
- We use the prefixes `[ENG]`, `[UX]` and `[Product]` to indicate their area of focus. The prefixes can be combined if the epic holds issues of different areas, e.g. `[ENG][UX]`.
- We use the labels `Engineering` and `UX` to easily filter epics.

#### Issue handover and breakdown

After a design is done, the design issue needs to be set to `workflow::planning breakdown` and engineering takes over the process of breaking it down. The design issue can be closed after break down is done.

#### How To Structure Epics

Epics can contain issues and/or child epics. A child epic could for example be the first iteration of the parent epic.
An example of how the structure of an epic could look:

- Parent Epic
  - Child Epic 1: First Iteration
    - Issue: Design Task 1
    - Issue: Design Task 2
    - Issue: Engineering Task 1
    - Issue: Engineering Task 2
  - Child Epic 2: Second Iteration
    - Issue: Design Task 3
    - Issue: Engineering Task 3
  - Child Epic 3: API Changes
    - Issue: Engineering Task 4
    - Issue: Engineering Task 5
  - Issue: Engineering Task 6
  - Issue: Engineering Task 7

#### Using epics across groups

Epics have the following limitations:

- Epics can only link issues that are within the same group. For example, it's not possible to link an issue in `gitlab-org` from an epic created in `gitlab-org/growth`.
- Epics can't link issues across different top level groups. For example, an epic created in `gitlab-org` can't link to an issue created in `gitlab-services`.

To overcome this, we will:

- Always create epics in the top level group, e.g. [`gitlab-org`](https://gitlab.com/groups/gitlab-org/-/epics`), [`gitlab-com`](https://gitlab.com/groups/gitlab-com/-/epics`), or [`gitlab-services`](https://gitlab.com/groups/gitlab-services/-/epics`).
- Create placeholder epics that link to the other top-level group epic. These placeholders get created automatically when pasting a link to an epic of another top-level group epic

The parent epic should live on the top-level group where most of the issues and child epics will be created.

### Issue labels

We use issue labels to keep us organized. Every issue has a set of required labels that the issue must be tagged with. Every issue also has a set of optional labels that are used as needed.

**Required labels**
- [Stage:](/handbook/engineering/development/growth/#how-we-work) `~devops::growth`
- [Group:](/handbook/engineering/development/growth/#how-we-work) `~group::product intelligence`
- [Workflow:](/handbook/product-development-flow/#workflow-summary) `~"workflow::planning breakdown`, `~"workflow::ready for development`, `~"workflow::in dev`, etc.

**Optional labels**
- [Experiment:](/handbook/engineering/development/growth/#experiment-issue-creation) `~growth::experiment`
- [Experiment Status:](/handbook/engineering/development/growth/#experiment-issue-creation) `~"experiment::active`, `~"experiment::validated`, etc.
- [Release Scoping:](https://gitlab.com/gitlab-org/gitlab-foss/blob/master/doc/development/contributing/issue_workflow.md) `~Deliverable`
- [UX:](/handbook/engineering/development/growth/#ux-workflows) `UX`
- Other labels in [issue workflow](https://gitlab.com/gitlab-org/gitlab-foss/blob/master/doc/development/contributing/issue_workflow.md)

### Merge request labels

MR labels can mirror issue labels (which is automatically done when created from an issue), but only certain labels are required for correctly [measuring engineering performance](#measuring-engineering-performance).

**Required labels**
- [Stage:](/handbook/engineering/development/growth/#how-we-work) `~devops::growth`
- [Group:](/handbook/engineering/development/growth/#how-we-work) `~group::product intelligence`
- [Type:](/handbook/engineering/metrics/#data-classification) `~security`, `~bug`, `~feature`, `~tooling`, `~documentation`

### Milestones

We tag each issue and MR with the planned milestone or the milestone at time of completion.

## Team Meetings

Our group holds synchronus meetings to gain additional clarity and alignment on our async discussions. We aim to record all of our meetings as our team members are spread across several timezones and often cannot attend at the scheduled time.

### Meeting rules

* Agenda items should be filled in 6 hours before meetings otherwise it's possible to cancel the meeting.
* It's fine to add agenda items during the meeting as things come up in sync meetings we might not have thought about beforehand.
* Meetings start :30 seconds after start time
* Whoever has the first agenda item starts the meeting.
* Whoever has the last agenda item ends the meeting.
* Meetings end early or on time.
* Any missed agenda items are bumped to the next meeting.

### Our meetings

* **Product Intelligence Sync:** an optional weekly meeting for the Product Intelligence team to discuss any topics they please.
* **Product Intelligence and Data Sync:** an optional weekly meeting for the Product Intelligence and Data teams to discuss cross-functional initiatives the teams are working on.
* **Usage Ping Pairing:** an optional weekly meeting for engineers to pair program with each other on our Usage Ping tooling.
* **Snowplow Pairing:** an optional weekly meeting for engineers to pair program with each other on our Snowplow tooling.
* **Product Intelligence Team Social:** an optional bi-weekly call for our team to hang out and socialize.

## Daily standups

We have daily asynchronous standups. Team members are using either [status hero](https://statushero.com) or [geekbot](https://geekbot.com/) for their daily standups. The purpose of these standups are to allow team members to have visibility into what everyone else is doing, allow a platform for asking for and offering help, and provide a starting point for some social conversations.

Three questions are asked at each standup:
* How do you feel today?
* What are you working on today?
  * Our status updates consist of the various issues and merge requests that are currently being worked on. If the work is in progress, it is encouraged to share details on the current state.
* Any blockers?
  * We raise any blockers early and ask for help.

## Measuring Engineering Performance

See the [Growth Section Performance Indicators](/handbook/engineering/development/performance-indicators/growth/) as well as the
[Centralized Engineering Dashboards](/handbook/engineering/metrics/).

We recognize that just as [an issue may be broken down into multiple merge requests](#ratio-of-issues-to-mrs), so can iteration of a feature be spread across several MRs, especially with the use of [feature flags](https://docs.gitlab.com/ee/development/feature_flags/process.html#feature-flags-in-gitlab-development).

We aim for the current [development department merge request rate](/handbook/engineering/development/performance-indicators/#development-department-mr-rate)
which is tracked using our [Growth Section Performance Indicators](/handbook/engineering/development/performance-indicators/growth/).

## Onboarding

All new team members to the Product Intelligence teams are provided two onboarding issues to help ramp up on our analytics tooling. New team member members should create their own onboarding issues in the [gitlab-org/growth/team-tasks project](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues). Note that this template lives in our handbook page instead of inside a GitLab template as it provides better visibility for incoming team members.

### Usage Ping Onboarding Template

* Issue Title: Send and receive a Usage Ping from GitLab to Versions app locally
* Example Issue: [Product Intelligence Usage Ping Onboarding Issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/304)

```
## Overview

The goal of this issue is to introduce you to how usage ping works. Your first task is to locally replicate the sending and receiving of a Usage Ping. In order to do this, you will traverse the gitlab and versions codebases to see how a usage ping is sent and collected.

Please work with your onboarding buddy if you have any questions.

## Steps
- [ ] Read the [Product Intelligence Guide](https://about.gitlab.com/handbook/product/product-intelligence-guide/)
- [ ] Read the [Usage Ping Guide](https://docs.gitlab.com/ee/development/usage_ping/)
- [ ] Clone and start https://gitlab.com/gitlab-org/gitlab
- [ ] Clone and start https://gitlab.com/gitlab-services/version-gitlab-com
- [ ] Setup versions to listen for incoming usage pings
- [ ] Point gitlab to the versions endpoint instead of the default endpoint
- [ ] In gitlab via rails console, manually trigger a usage ping
- [ ] In versions via rails console, check that a usage ping was successfully received, parsed, and stored in the Versions database.
- [ ] Notify your manager and onboarding buddy once this issue is complete
- [ ] Ask your manager to add you to the [/gitlab-org/growth/product-intelligence/engineers/](https://gitlab.com/groups/gitlab-org/growth/product-intelligence/engineers/-/group_members) so that you can start doing [Product Intelligence reviews](https://docs.gitlab.com/ee/development/usage_ping/#9-ask-for-a-product-intelligence-review)
- [ ] Close off this issue
```

### Snowplow Onboarding Template

* Issue Title: Send and receive Snowplow Events from GitLab to Snowplow Micro
* Example Issue: [Product Intelligence Snowplow Onboarding Issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/316)

```
## Overview

The goal of this issue is to introduce you to how Snowplow works. Your first task is to locally replicate the sending and receiving of Snowplow events. In order to do this, you will traverse the gitlab and snowplow codebases to see how a snowplow event is sent and collected.

Please work with your onboarding buddy if you have any questions.

## Steps
- [ ] Read the [Product Intelligence Guide](https://about.gitlab.com/handbook/product/product-intelligence-guide/)
- [ ] Read the [Snowplow Guide](https://docs.gitlab.com/ee/development/snowplow/index.html)
- [ ] Clone and start GitLab https://gitlab.com/gitlab-org/gitlab
- [ ] Clone and read through the readme for Snowplow Iglu https://gitlab.com/gitlab-org/iglu
- [ ] Clone and start Snowplow Micro https://docs.gitlab.com/ee/development/snowplow/index.html#snowplow-micro
- [ ] Add a Snowplow event using HAML https://docs.gitlab.com/ee/development/snowplow/index.html#tracking-in-haml-or-vue-templates
- [ ] Add a Snowplow event using Ruby https://docs.gitlab.com/ee/development/snowplow/index.html#implementing-snowplow-ruby-backend-tracking
- [ ] Using your browser, navigate to wherever the event was added and trigger all the added Snowplow events (HAML, Ruby)
- [ ] In Snowplow Micro, ensure all of the above mentioned events are successfully captured as good events in `localhost:9090/micro/good`
- [ ] Notify your manager and onboarding buddy once this issue is complete
- [ ] Ask your manager to add you to the [/gitlab-org/growth/product-intelligence/engineers/](https://gitlab.com/groups/gitlab-org/growth/product-intelligence/engineers/-/group_members) so that you can start doing [Product Intelligence reviews](https://docs.gitlab.com/ee/development/usage_ping/#9-ask-for-a-product-intelligence-review)
- [ ] Close off this issue
```

## Quick Links

| Resource                                                                                                                          | Description                                               |
|-----------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------|
| [Product Intelligence Guide](/handbook/product/product-intelligence-guide)                                                                              | A guide to Product Intelligence                   |
| [Usage Ping Guide](https://docs.gitlab.com/ee/development/usage_ping/)                                                        | An implementation guide for Usage Ping                    |
| [Snowplow Guide](https://docs.gitlab.com/ee/development/snowplow/index.html)                                                            | An implementation guide for Snowplow                      |
| [Event Dictionary](/handbook/product/product-intelligence-guide#event-dictionary)                                        | A SSoT for all collected metrics and events               |
| [Privacy Policy](/privacy/)                                                                                                       | Our privacy policy outlining what data we collect and how we handle it     |
| [Implementing Product Performance Indicators](/handbook/product/product-intelligence-guide#implementing-product-performance-indicators)                                   | The workflow for putting product performance indicators in place   |
| [Product Intelligence Direction](/direction/product-intelligence/)                                                                              | The roadmap for Product Intelligence at GitLab                       |
| [Product Intelligence Development Process](/handbook/engineering/development/growth/product-intelligence/) | The development process for the Product Intelligence groups         |
| [Growth Product Direction](/direction/growth/)                                                                                    | The roadmap for Growth at GitLab                          |
| [Growth Product Handbook](/handbook/product/growth/)                                                                              | The product process for the Growth sub-department         |
| [Growth Sub-Department Development Process](/handbook/engineering/development/growth/).                                              | The development process for the Growth sub-department     |
| [Growth Sub-Department Performance Indicators Process](/handbook/engineering/development/performance-indicators/growth/)              | The performance indicators for the Growth sub-department  |
| [Growth UX Process](/handbook/engineering/ux/stage-group-ux-strategy/growth/)                                                        | The UX process for the Growth sub-department              |
| [Growth QE Process](/handbook/engineering/quality/growth-qe-team/)                                                                   | The QE process for the Growth sub-department              |
| [GitLab Performance Snowplow Dashboards ](./gitlab_com_performance_dashboard.html) | Performance dashbaords for GitLab.com via Snowplow  |

