---
layout: handbook-page-toc
title: Trust & Safety Team 
description: "GitLab.com Trust & Safety Team Overview" 
---

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

----

## <i class="far fa-paper-plane" id="biz-tech-icons"></i> Reaching out to Us


<div class="flex-row" markdown="0">
  <a href="/handbook/engineering/security/security-operations/trustandsafety/index.html#-contact-us" class="btn btn-purple-inv" style="width:170px;margin:5px;height:100%;display: flex;align-items:center;justify-content:center;">Contact us</a>
  </div>

---

## <i class="fas fa-tasks" id="biz-tech-icons"></i> Our Vision

Making the internet safer by reducing malicious activity originating from GitLab.com.
 
## <i class="fas fa-rocket" id="biz-tech-icons"></i> Our Mission

The Trust & Safety team are the guardians of the anti-abuse world, who develop the tools and manage the workflows to mitigate [abusive activity](#what-is-abuse) on GitLab.com.

The Trust & Safety team investigates and mitigates the malicious use of GitLab.com and it’s associated features and tools with the goal of making the internet a safer place. In order to achieve this we must ensure that we are good internet citizens. 

#### Our Mission statement

Be good internet citizens.

### How do we achieve this? 

* Be honest and trustworthy
* Follow the rules and laws
* Be informed about the world around you
* Respect the property of others
* Be active in the community
* Be compassionate
* Be a good neighbour
* Protect the GitLab environment
* Respect the rights of others
* Take responsibility for your actions

---

## <i class="fas fa-receipt" id="biz-tech-icons"></i> Initiatives for this specialty include:

* Detection and mitigation of [abusive activity - see the [common-forms-of-abuse](/handbook/engineering/security/security-operations/trustandsafety/#what-is-abuse)` section.
* Processing of DMCA Notices and Counter-Notices - See [DMCA-Requests](/handbook/engineering/security/security-operations/trustandsafety/#dmca-requests)` section below.  
* Escalating potential abuse vectors to stakeholders.
* Research and prevention trending abuse methodologies on GitLab.com.
* ***Code of Conduct Violations*** Open up a `confidential` issue in the Operations Tracker for - For more information on reporting these violations please see the [GitLab Community Code of Conduct](https://about.gitlab.com/community/contribute/code-of-conduct/) page.


## <i class="fas fa-users" id="biz-tech-icons"></i> The Team 

### Team Members

The following people are permanent members of the Trust & Safety Team

<table>
<thead>
<tr>
<th>Person</th>
<th>Role</th>
</tr>
</thead>
<tbody>
<tr>
<td>Roger Ostrander</td>
<td><a href="/job-families/engineering/trust-and-safety">Security Engineer, Trust & Safety</a></td>
</tr>
<tr>
<td>Shawn Sichak</td>
<td><a href="/job-families/engineering/trust-and-safety">Security Engineer, Trust & Safety</a></td>
</tr>
<tr>
<td>Westley van den Berg</td>
<td><a href="/job-families/engineering/trust-and-safety">Security Analyst, Trust & Safety</a></td>
</tr>
<tr>
<td>Charl de Wit</td>
<td><a href="/job-families/engineering/trust-and-safety">Security Manager, Trust & Safety</a></td>
</tr>
</tbody>
</table>


## <i class="fas fa-stream" id="biz-tech-icons"></i> Contact Us

<div class="flex-row" markdown="0">

  <a href="/handbook/support/workflows/reinstating-blocked-accounts.html" class="btn btn-purple-inv" style="width:170px;margin:5px;height:100%;display:flex;align-items:center;justify-content:center;">Blocked Accounts</a>
  <a href="/handbook/engineering/security/security-operations/trustandsafety/diy.html" class="btn btn-purple-inv" style="width:170px;margin:5px;height:100%;display: flex;align-items:center;justify-content:center;">Open an issue</a>
  <a href="https://gitlab.com/gitlab-org/gitlab/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=#" class="btn btn-purple-inv" style="width:170px;margin:5px;height:100%;display:flex;align-items:center;justify-content:center;">Feature Requests</a>
  </div>

---

* GitLab team members can tag `(@gitlab-com/gl-security/security-operations/trust-and-safety)` us on any issue they require our input on, or create an issue in our Operations Issue tracker. If it's a time sensitive issue, please reach out on Slack in the #abuse channel or by using @trust-and-safety.

* For any abuse prevention feature requests and suggestions for `Gitlab.com`, `CE` and `EE`, please create a Feature proposal Issue from the provided templates in the GitLab Project and add the ~"Abuse Prevention" label. Feel free to us for additional input @gitlab-com/gl-security/security-operations/trust-and-safety or if you have any questions.

* If you are running your own GitLab instance and are looking for some pro-tips on dealing with abuse, visit the [Trust & Safety Do It Yourself](/handbook/engineering/security/security-operations/trustandsafety/diy.html) **DIY Gitlab CE/EE** page.

* For **Open Source Program Partners**, `Premium` and `Ultimate` customers, you can reach us at `abuse@gitlab.com` if you would like to discuss, and potentially improve, your current spam prevention and mitigation strategies. 
  Include any relevant information as to the abuse issue you are currently experiencing to help us provide the most relevant information.

---

## <i class="fas fa-book" id="biz-tech-icons"></i> Dashboards and Frequently visited pages

<div class="flex-row" markdown="0">
  <a href="/handbook/engineering/security/security-operations/trustandsafety/diy.html" class="btn btn-purple-inv" style="width:170px;margin:5px;height:100%;display: flex;align-items:center;justify-content:center;">DIY (Gitlab CE/EE)</a>
  <a href="/handbook/engineering/security/security-operations/trustandsafety/dmca-removal-requests.html" class="btn btn-purple-inv" style="width:170px;margin:5px;height:100%;display: flex;align-items:center;justify-content:center;">DMCA Workflow</a>
  <a href="https://app.periscopedata.com/app/gitlab/780726/WIP:-Blocked-User-Usage-Data" class="btn btn-purple-inv" style="width:170px;margin:5px;height:100%;display:flex;align-items:center;justify-content:center;">Blocked Users</a>
  <a href="https://app.periscopedata.com/app/gitlab/761157/Cost-of-Abuse" class="btn btn-purple-inv" style="width:170px;margin:5px;height:100%;display:flex;align-items:center;justify-content:center;">Cost of Abuse</a>
  </div>

--- 

## <i class="far fa-flag" id="biz-tech-icons"></i> Reporting Abuse

### How to report Abuse

You can report abuse on GitLab.com via the `Report Abuse` [button](https://docs.gitlab.com/ee/user/report_abuse.html) while logged into your gitlab.com account.
	
  * Please ensure to include any relevant details pertaining to your report in the text field.
  * You can submit more detailed reports via email to `abuse@gitlab.com`
  * For DMCA Notices please email `dmca@gitlab.com`
  * The Trust and Safety [Workflows](/handbook/engineering/security/security-operations/trustandsafety/dmca-removal-requests.html) 

---

## <i class="fas fa-info-circle" id="biz-tech-icons"></i> Other Abuse Reports

Non Gitlab Members  can submit more detailed reports about [abuse](#what-is-abuse) originating from or hosted on Gitlab.com, see our [**How to Report Abuse**](#how-to-report-abuse) section below.

GitLab team members can tag (`@gitlab-com/gl-security/security-operations/trust-and-safety`) us on any issue they require our input on, or create an issue in our [Operations Issue tracker](https://gitlab.com/gitlab-com/gl-security/security-operations/trust-and-safety/operations/-/issues). If it's a time sensitive issue, please reach out on Slack in the `#abuse` channel or by using `@trust-and-safety`. 

---

## <i class="fas fa-bullseye" id="biz-tech-icons"></i> DMCA Notices & other disputes
 
### DMCA Requests

We take the intellectual property rights of others seriously and require that our Users do the same. The Digital Millennium Copyright Act (DMCA) established a process for addressing claims of copyright infringement.

The Trust & Safety team are responsible for processing Digital Millennium Copyright Act (DMCA) notices.  

* DMCA Requests can be sent to *dmca@gitlab.com*
* Abuse reports can be sent to *abuse@gitlab.com*
* Accounts can be reported using the Report `ReportAbuse` dropdown menu or button.
* Account Reinstatements can be requested by using the `Account Reinstatement` button at the top of this page
* Our Terms can be found at `https://about.gitlab.com/terms`

### DMCA Notice Workflow
 
* Confirm that the content is still available. 
* Confirm that the DMCA notice meets our [DMCA Policy](/handbook/dmca/) requirements. 

For more detailed information regarding how we process DMCA related notices, please see: [DMCA Removal Workflow](/handbook/engineering/security/security-operations/trustandsafety/dmca-removal-requests.html)

### Other disputes

Read more about [Namespace and Trademark](https://about.gitlab.com/solutions/open-source/partners/) and [Ownership Dispute](/handbook/support/#ownership-disputes) policies.

Disputes regarding namespaces, ownership and trademarks are not governed by DMCA. These disputes must be resolved by the parties involved. GitLab will never act as arbitrators or intermediaries in these disputes and will not take any action without the appropriate legal orders.

## Useful Links and other information
 
For [Open Source Program Partners](https://about.gitlab.com/solutions/open-source/partners/), Premium and Ultimate customers, you can reach us at `abuse@gitlab.com` if you would like to discuss, and potentially improve, your current spam prevention and mitigation strategies.

## <i class="far fa-newspaper" id="biz-tech-icons"></i> Common forms of Abuse

### What is Abuse

Abuse is the intentional misuse of GitLab products/services to cause harm or for personal gain. (Abuse is covered under Section 3 of the GitLab Website [Terms of Use](/terms/#gitlab_com))

#### Examples of common forms of Abuse include, but are not limited to:
**1. Malware:** Defined as software that is designed and distributed with the intention of causing damage to a computer, server, client, or computer network.

Making use of GitLab.com services to deliver malicious executables or as attack infrastructure is prohibited under the [GitLab Website Terms of Use](/terms/#gitlab_com) (**Section 3, “Acceptable Use of Your Account and the Website”**).
We do however understand that making such technical details available for research purposes can benefit the wider community and as such it will be allowed if the content meets the following criteria:
  
  - The Group and Project descriptions must clearly describe the purpose and author of the content.
  - Further details about specific project content that can be independently verified by the **GitLab Security** department must be present in the project `README.md` file; for example, links to supporting materials such as a blog post describing the project.
  - All malicious binaries/files are stored in password protected archive files, with the passwords clearly documented; for example, placed in the repository’s `README.md`.
     * Example: https://github.com/ytisf/theZoo
     * `git-lfs` is available for use for binary files on GitLab.com.

  - Non-profit open source projects may meet the requirements to qualify for our [GitLab for Open Source](https://about.gitlab.com/solutions/open-source/partners/) program.

**2. Commercial Spam:** An account that's been created for the purpose of advertising a product or service.

**3. Malicious Spam:** An account that’s been created for the purpose of distribution of fraudulent, illegal, pirated or deceptive content.

**4. CI Abuse:** Making use of CI Runners for any other purpose than what it is intended for. Examples include, but are not limited to:
  - Cryptocurrency Mining
  - Network Abuse (Denial of Service, Scraping, etc.)

**5. Prohibited Content:** Distributing harmful or offensive content that is defamatory, obscene, abusive, an invasion of privacy (Personally Identifiable Information/PII) or harassing.
  - This includes distributing PII obtained from a data breach.
  - For any reports of Child Sexual Abuse Material (CSAM) please notify [INHOPE](https://www.inhope.org/EN) via the [`Report Content`](https://www.inhope.org/EN#hotlineReferral) button.

**6. GitLab Pages:** Pages Abuse: Include, but are not limited to:
  - Web Spam,
  - Phishing Pages
  - Pages that contain Obscene or Harmful content
  - Copyright Content

**7. Gitlab Wiki:** Wiki Abuse: Include but not limited
  - Phishing Pages
  - Pages that contain Obscene or Harmful content
  - Copyright Content
  - Harmful/Malicious Downloadable content

