---
layout: handbook-page-toc
title: "All-Remote Meetings"
canonical_path: "/company/culture/all-remote/meetings/"
description: How to conduct meetings in a remote company
twitter_image: "/images/opengraph/all-remote.jpg"
twitter_image_alt: "GitLab remote team graphic"
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---

## On this page

{:.no_toc}

- TOC
{:toc}

## Introduction

On this page, we're detailing how to optimize meetings in an all-remote environment.

## How do you do all-remote meetings right?

![GitLab marketing team Show & Tell social call](/images/all-remote/gitlab-zoom-meeting-hat-filter.jpg)
{: .shadow.medium.center}

If you're going to have a meeting, at least have a little fun
{: .note.text-center}

"How do you do meetings right?" is a common question asked of all-remote companies like GitLab. The below assumes you've already questioned whether the meeting should happen in the first place; a guide to that selection process is found in GitLab's [guide to asynchronous workflows and communication](/company/culture/all-remote/asynchronous/#question-every-meeting).

The truth is that much of the same advice applicable to in-person meetings apply to meetings within an all-remote company, with a few notable distinctions.

### Make meeting attendance optional

When you work in a global all-remote company, the usual assumptions about availability are opposite the norm. We have a growing team working in over 65 countries, with many time zones covered, which makes synchronous meetings impractical, burdensome, and inefficient. Anyone who has worked in a corporate environment has likely seen the sarcastic "I Survived Another Meeting That Should Have Been An Email" award. As an all-remote company, we [do not look to a meeting by default](/company/culture/all-remote/asynchronous/#question-every-meeting); when they are necessary, we strive to make in-person attendance optional by enabling asynchronous contribution.

In many companies, synchronous meetings are used as a mechanism to create consensus. As you'll read in the [Leadership](/handbook/leadership/) portion of GitLab's handbook, we are not a democratic or consensus driven company. People are encouraged to give their comments and opinions, but in the end one person decides the matter after they have listened to all the feedback.

This works because of our values, which leads GitLab to hire individuals who enjoy being a manager of one, a point detailed in our [Efficiency value](/handbook/values/#efficiency). Team members are empowered to decide whether a sync meeting is the best use of their time, and [set boundaries](/company/culture/all-remote/mental-health/#be-transparent-about-boundaries) when needed.

You should aim to record all meetings, particularly when key individuals aren't able to join live. This allows team members to catch up on what transpired, adding context to notes that were taken during the meeting. Learn more about recording in Zoom in the [Tips and Tricks section of GitLab's Handbook](/handbook/tools-and-tips/#recording-in-zoom).

### Using GitLab to replace meetings

A major enabler for reducing the quantity of meetings necessary at GitLab is our own product. GitLab is a collaboration tool designed to help people work better together whether they are in the same location or spread across multiple time zones. Originally, GitLab let software developers collaborate on writing code and packaging it up into software applications. Today, GitLab has a wide range of capabilities used by people around the globe in all kinds of companies and roles.

You can learn more at GitLab's [remote team solutions page](/solutions/gitlab-for-remote/).

### Live doc meetings

![GitLab all-remote mentor](/images/all-remote/ceo-shadow-gitlab-awesomeness.jpg)
{: .shadow.medium.center}

Every work-related meeting should have a live doc agenda affixed to the calendar invite. To better understand how GitLab utilizes agenda docs, here's a [templated example](https://docs.google.com/document/d/1WQe-0oiMCzB3MPBNdKluCEIfgTRpaIi-SJ8FmUJ2xHo/edit?usp=sharing) you can copy and use in your organization.

**If you determine that a meeting is needed to move a project forward, address a blocker, or resolve a miscommunication, visit our detailed guide on conducting a [live doc meeting](/company/culture/all-remote/live-doc-meetings/).**

### Document everything live (yes, everything)

It's not rude to focus on documentation in a meeting. A surefire way to waste time in a meeting is to avoid writing anything down. Meetings within an all-remote company require documentation to be worthwhile.

1. During the meeting, add input and feedback from attendees to existing agenda items.
1. For action items, we [go directly to a GitLab issue](/blog/2016/03/03/start-with-an-issue/) or [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/getting_started.html). This creates a direct takeaway from the meeting, where ideas are summarized and action can begin immediately.
1. For optional attendees, or key team members who could not attend the meeting live, tagging them in the resulting GitLab issue(s) enables them to get themselves up to speed and contribute when it is suitable for their schedule.
1. Record the meeting. You can always delete an unwanted meeting, but you cannot turn back the clock and record a meeting retroactively. This is particularly important if you want to present or wish to have a written transcription of the meeting. Zoom's [Cloud Recording](https://support.zoom.us/hc/en-us/articles/115004794983-Automatically-Transcribe-Cloud-Recordings-) supports transcription natively, and [Otter](https://otter.ai/) is another popular transcription tool.

<!-- blank line -->

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/N3COqJvme-Q?start=41" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- blank line -->
You can see this in practice by viewing past [GitLab Group Conversations](/handbook/people-group/group-conversations/) on our [GitLab Unfiltered YouTube channel](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A/search?query=group+conversation).

### Cancel unnecessary recurring meetings

Recurring meetings are oftentimes established as meaningful points along a given a journey. Don't hesitate to cancel them after their purpose has been served. Cancelling meetings isn't a slight to those on the invite list. In fact, ridding multiple calendars of a meeting should be celebrated and conjure a sense of liberation.

### Use the right tools

<!-- blank line -->

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/Kf0QOihrxLg" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- blank line -->
All-remote meetings are made simpler given that there's no jockeying for space in a boardroom, scrounging for huddle rooms, or wondering if a given group still needs the meeting room they've reserved — all very real conundrums in colocated environments.

1. GitLab uses Zoom for video calls and screensharing. Its simple recording function makes it easy to capture meetings for others to watch at a later time. Learn more about how we optimize Zoom usage in our meetings in the [Tools and Tips](/handbook/tools-and-tips/#zoom) portion of our Handbook.
1. We use [GitLab Issues](https://docs.gitlab.com/ee/user/project/issues/) to document action items that come out of any given meeting, and loop anyone else in who opted out of real-time attendance.
1. Leverage tools such as Calendly, which can show you as busy in chat tools like Slack.

### Meetings are about the work, not the background

<!-- blank line -->

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/EeUhxQn_ct4" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- blank line -->
In the above [video interview](https://youtu.be/EeUhxQn_ct4) between GitLab co-founder and CEO [Sid Sijbrandij](https://twitter.com/sytses) and [NoHQ](https://nohq.co/)'s [Dominic Monn](https://twitter.com/dqmonn), the two discuss common challenges and solutions to building, sustaining, and scaling a thriving remote workplace.

They speak at length about meetings within a remote environment. In particular, Sid shares that remote workers should embrace the benefit of being free to take meetings with loved ones nearby.

> **Enjoy the benefits of your kids barging in on a meeting. That's the best distraction in the world.** - _GitLab co-founder and CEO [Sid Sijbrandij](https://twitter.com/sytses)_

One's appearance, surroundings, and background can be the source of great stress and anxiety when preparing for a video call. At GitLab, we encourage team members to bring their whole selves to work.

1. Don't waste time trying to find the perfect backdrop for your video call.
1. Celebrate [unique surroundings](/blog/2019/06/25/how-remote-work-at-gitlab-enables-location-independence/). It's not uncommon to see GitLab team members participate in a video call from the shores of a lake, coffee shops, RVs, or even while walking.
1. Focus on your internet connection and your audio quality ([use headphones](/blog/2019/06/28/five-things-you-hear-from-gitlab-ceo/)), reducing listening fatigue for others.
1. Encourage others to say hello! All-remote employees invite others into their homes via video call, creating a unique opportunity to share their lives with colleagues.
1. Consider _not_ using a video call. Visit GitLab's [Communication](/handbook/communication/#video-calls) section of the Handbook to learn more.

### Avoid hybrid calls

A hybrid call is one that has a mix of participants in the same physical room, together with others who are remote. Hybrid calls should be avoided, as it's better to have everyone on a level playing field for communication and discussion. If a hybrid call must happen, however, everyone should use their own equipment (camera, headset, screen) even if they are physically sitting in the same room. This ensures that everyone is on the same playing field in terms of call experience. If possible, it's best to separate briefly for the call and find your own workspace, creating a 100% remote call. This helps avoid audio problems from delays and feedback. Learn more about why hybrid (partially remote) calls are horrible in the [Communication section of GitLab's Handbook](/handbook/communication/#hybrid-calls-are-horrible).

### Start on time, end on time

A nontrivial amount of time is required to regain focus on a task following a distraction. While it is not always possible to schedule meetings such that they do not break up the flow of an ongoing project, it's important to begin and end meetings on time as to minimize disruption.

When scheduling a meeting we value people's time and prefer the "speedy meetings" setting in our Google Calendar. This gives us meetings of, for example, 25 or 50 minutes leaving some time to write notes, stretch, etc. before continuing to our next call or meeting. (This setting can be found under the calendar Settings menu at "default event duration"). Learn more in the [Communication section of GitLab's Handbook](/handbook/communication/#scheduling-meetings).

### It's OK to look away

Many organizations have attempted to improve the utility of meetings — usually as a workaround to actually doing less of them — by implementing a "screen-free" meeting mandate. At GitLab, we empower team members to be the manager of their attention.

1. It's completely acceptable to work on other tasks if (a particular portion of) a meeting doesn't apply to you. If your meeting is with only GitLab team members please leave your camera on to allow people to see the overall interest level and adjust the time spent on certain topics. If your meeting includes non-GitLab team members consider turning off your camera because people outside GitLab might not know that it is acceptable for us to work on other tasks during a meeting.
1. It's not embarrassing to ask occasionally for something to be repeated. Because you manage your own attention, you are free to engage with other work and then be pulled back into a relevant part of the meeting conversation.
1. It feels rude in video calls to interrupt people. This is because the latency causes you to talk over the speaker for longer than during an in-person meeting. We should not be discouraged by this, the questions and context provided by interruptions are valuable. This is a situation where we have to do something counter-intuitive to make all-remote meetings work. In GitLab, everyone is encouraged to interrupt the speaker in a video call to ask a question or offer context. We want everyone to contribute instead of a monologue. Just like in-person meetings be cognizant of when, who, and how you interrupt. For example, this can help avoid the issue of [men too often interrupting women](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.870.5508&rep=rep1&type=pdf).

Learn more in the [Communication section of GitLab's Handbook](/handbook/communication/#video-calls)

### Say thanks and be creative

At GitLab, we have a dedicated Slack channel [devoted to saying thanks](/handbook/communication/#say-thanks). Kindness is embedded in our [Collaboration](/handbook/values/#collaboration) value, and gratitude is an essential part of our culture. This reinforces what connects us as a [geographically diverse](/company/culture/inclusion/#fully-distributed-and-completely-connected) team.

A great example of several GitLab's [values](/handbook/values/) being used to generate a creative outcome from a regularly scheduled meeting is detailed on the company blog: ["How we turned a dull weekly all-hands into a podcast"](/blog/2019/06/03/how-we-turned-40-person-meeting-into-a-podcast/)

## All-remote Virtual "Offsites"

![GitLab values illustration](/images/all-remote/gitlab-values-tanukis.jpg)
{: .medium.center}

When working in an all-remote company, there is a strong tendency to avoid traditional "offsites" that require travel from all participants. GitLab has [experimented](https://gitlab.com/gitlab-com/Product/issues/601) with all-remote "offsites" (AROs) as a method to provide some of the deeper shared understanding that results from such meetings without the heavy financial and personal toll of travel. These longer, structured meetings have had mixed results. Here's what we've learned so far:

1. Block off everyone's calendar just as if they'd traveled for an offsite. Remove all other distractions (Slack, email, etc.) and enable Do Not Disturb modes on phones and computers.
1. Ensure you have a strong desired outcome before planning an ARO. This will ideally be discussed before the date and time are set, and placed atop the [Google Doc agenda](/handbook/communication/#google-docs) within the calendar invite.
1. Coordinate pre-reading and work that can be done [asynchronously](/handbook/communication/#internal-communication) ahead of the ARO.
1. For newly formed groups, include initial ice-breakers and activities to develop shared trust. For example, begin with a show & tell that allows team members to showcase something they've assembled or have contributed to.
1. Schedule breaks during calls longer than 1 hour.
1. Keep in mind the downsides of time-zone shifting to attend these synchronous meetings. Team members in shifted time-zones may arrive with lower energy.
1. Combine asynchronous activities between synchronous meetings.
1. Take more time than you normally would to explain activities and intent, framing your position with [additional context](/company/culture/all-remote/effective-communication/). In traditional offsites, understanding came come from observing others in the room, and this can be more difficult remotely. If you are leading the offsite, encourage people to have [short toes](/handbook/values/#short-toes) and [assume positive intent](/handbook/values/#assume-positive-intent) when it comes to being interrupted, as a higher-than-expected amount of back-and-forth may be necessary to achieve understanding.
1. If possible, spend a full day working together with breaks, and share a remote meal together [coffee-chat](/company/culture/all-remote/informal-communication/#coffee-chats) style.
1. Check your [tools](/handbook/tools-and-tips/) to make sure they can support the synchronous setup you are proposing.

All-remote virtual offsites are cost-effective and enable team members to pivot back to [life outside of work](/company/culture/all-remote/people/#worklife-harmony) as soon as the meeting concludes. However, there is still great value in [in-person interactions](/company/culture/all-remote/in-person/), and leaders should aim to include those opportunities when possible rather than shifting entirely to remote offsites.

## Running a remote board meeting

In April 2019, GitLab transitioned our board meetings to all-remote. This means that none of the attendees are colocated. By doing so, we've made it as easy as possible for the right people to attend our board meetings, including board members, observers, executives, and anyone doing a [deep dive](/handbook/board-meetings/#deep-dives), which can include directors, managers, and, in some cases, individual contributors.

Learn more on [how to run an all-remote board meeting](/blog/2020/04/15/remote-board-meeting/).

## Comparing remote and colocated meetings

<!-- blank line -->

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/EuGsen3FxXc?start=788" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- blank line -->
When asked during an [INSEAD](http://insead.edu/) case study interview (shown above) about an all-remote company's ability to bring people together in the same physical space for a meeting, GitLab co-founder and CEO Sid Sijbrandij provided the following reply.

For context, Sid joined the Zoom call from San Francisco, while the researchers joined from Singapore.

> We can [bring people together in the same physical space], but we don't do it because it's very inefficient. Imagine the cost of this meeting if I had to fly to your location, or you to mine.
> 
> [Colocated companies] fall back on extremely inefficient things, like flying people halfway around the world for a four-hour meeting. GitLab doesn't fall into that trap.
> 
> We have meetings that are more efficient. I bet our meetings, via Zoom, are more efficient than meetings in a conference room. With [live note-taking](/handbook/communication/), up-front [agendas](/handbook/leadership/1-1/suggested-agenda-format/), and our follow-up, GitLab meetings are more efficient.
> 
> Not only do we have more efficient meetings, we also do not have the time waste of flying people across the world.
> 
> I see multinational organizations where you're supposed to be in the office even if you're the only person in that location, just so they can check that you're working. That is a ridiculous waste of time.
> 
> So, no, [colocated companies] do not have an advantage. They don't have the benefit of knowing how to do remote right because it's not in their DNA.
> 
> Of course there are benefits to colocated meetings — it's easier to interrupt each other, it's easier to see what everyone around the room is thinking, you don't have wireless issues, it's easier to look each other in the eyes, it's easier to break bread before/after the meeting, it's easier to talk a walk or do something fun together.
> 
> We try to take advantage of them, though. We have [GitLab Contribute](/events/gitlab-contribute/) where we all come together to go on excursions and have informal chats. As an executive group, we [come together every quarter for 2.5 days for high-velocity meetings](/company/offsite/). Those are augmented with Google Docs, and we allow people to attend remotely if they cannot join in person.
> 
> There are benefits, but they aren't as big as people make them out to be.
> 
> The biggest thing is [taking the initiative to interrupt each other](/blog/2019/08/05/tips-for-mastering-video-calls/), as that's harder in a remote setting. At GitLab, we solve that by making sure that questions are in a Google Doc in advance so it's clear who has a question and who to hand the conversation off to. — _GitLab co-founder and CEO Sid Sijbrandij_

## GitLab Knowledge Assessment: All-Remote Meetings

Complete all knowledge assessments in the [Remote Work Foundation certification](/company/culture/all-remote/remote-certification/) to receive the [Remote Foundations Badge in GitLab Learn](https://gitlab.edcast.com/pathways/copy-of-remote-foundations-badge). If you have questions, please reach out to our [Learning & Development team](/handbook/people-group/learning-and-development/) at `learning@gitlab.com`.

- - -

Return to the main [all-remote page](/company/culture/all-remote/).
