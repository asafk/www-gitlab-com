---
layout: solutions
title: GitLab on HashiCorp
description: "A complete DevOps platform to build, test, secure and deploy on HashiCorp Terraform"
canonical_path: "/partners/technology-partners/hashicorp/"
suppress_header: true
---

***
{:.header-start}

# GitLab on HashiCorp
{:.header-left}

Standardize secrets managment and secure GitOps workflows with technologies from GitLab and HashiCorp.
{:.header-left}

<p class="header-left"><a class="btn cta-btn orange" href="/sales/">Talk to an expert</a></p>


***
{:.header-end}

> From security scanning, managing access to senstive data, enfocing policy and goverance or lowering the barrier of adoption into GitOps and Infrastructure-as-Code (IaC) across workflows - go from idea to production faster with GitLab and HashiCorp.

>  GitLab is a complete DevOps platform, delivered as a single application and with product integrations between HashiCorp Vault and Terraform; GitLab can simplfy workflows across both DevSecOps and DevOps teams.

***

## Joint HashiCorp and GitLab Benefits

GitLab collapses cycle times by driving higher efficiency across all stages of the software development lifecycle running on HashiCorp. 

* Standardize Secret Management across Dev and Prod.
    * Vault can be configured as the general secrets management provider for GitLab across delivery into Dev and rollout into production. When quickly spinning up secure development environments, users can install Vault into a Kubernetes clusters from within GitLab to better align with real production environments.  
    * Practitioners using GItLab CI/CD can easily authenticate via JWT and securely consume variables, secrets, and even service account credentials and then fetch and read secrets from Vault as necessary.
* Versioning and collaboration with GitLab SCM + Terraform
    * Provide a single source of truth for Developer and Cloud Infrastructure Admins with GitLab Source Code Management. Store your Terraform plans and trigger Terraform Enterprise and Terraform Cloud pipelines in Git repositories and treat Terraform plans and Sentinel policies in GitLab like developers treat application source code. Empower cross functional teams with visibility with direct access to collaborate and contribute towards infrastructure declared in code via code reviews, Git commits, Merge Requests, and overall transparent iteration via Git.
* Automated speed, reliability, and consistency with GitLab CI/CD + Terraform Enterprise
    * Provide flexible, template driven modular workflows via GitLab CI/CD that evoke Terraform plans for infrastructure life cycle management. Furthermore, view CI/CD and Terraform Plan outputs within GitLab MRs for contextual changes associated with each change. Additionally, Ops teams can also configure GitLab CI/CD to evoke stored Sentinel policies in Git repositories for Policy as Code framework that provides access control, security, and operational guard rails within the automated workflow.  
{:.list-benefits}

***

## Joint Solution Capabilities with HashiCorp

* ![Terraform logo](/images/solutions/hashicorp/Terraform_PrimaryLogo_Black_RGB.svg)
  * GitLab and Terraform provide developers, operators, and practitioners alike with the ability to manage infrastructure automation for complex, distributed applications while maintaining security, compliance, and reliability. Organizations standardizing on Terraform and GitLab can simplify their technology stack for software and infrastructure lifecycle management and empower their engineers to collaborate, automate, and secure processes across multi-cloud environments. **[[Learn More about Terraform]](https://www.terraform.io/)**
* ![Vault logo](/images/solutions/hashicorp/Vault_PrimaryLogo_Black_RGB.svg)
  * Vault is a single security control plane for operations and infrastructure that many organizations have chosen to manage ACL, secrets and other sensitive data. Additionally, GitLab is a complete DevOps platform delivered as a single application for the SDLC process. Together, this joint solution provides an alternative to the error-prone, document-based collaboration methods used across teams and allows organization to migrate from bolting on security to shifting security left and can be configured together to standardize the security of secrets, tokens, credentials, etc. across development and production. 
. **[[Learn More about Vault]](https://www.vaultproject.io/)**
{:.list-capabilities}

---

## GitLab and HashiCorp Joint Solutions in Action
{:.no-color}

- [GitOps:The Future of Infrastructure Automation - A panel discussion with Weaveworks, HashiCorp, Red Hat, and GitLab](https://about.gitlab.com/why/gitops-infrastructure-automation/)
- [GitLab and HashiCorp: Providing application and infrastructure delivery workflows](https://about.gitlab.com/blog/2019/09/17/gitlab-hashicorp-terraform-vault-pt-1/)
- [How Wag! cut their release process from 40 minutes to just 6](https://about.gitlab.com/blog/2019/01/16/wag-labs-blog-post/)
- [GitLab and HashiCorp - A holistic guide to GitOps and the Cloud Operating Model](https://about.gitlab.com/webcast/gitlab-hashicorp-gitops/)
- [Empowering Developers and Operators through GitLab and HashiCorp](https://www.hashicorp.com/resources/empowering-developers-and-operators-through-gitlab-and-hashicorp/) 
- [HashiCorp Vault and GitLab integration: Why and How?](https://www.youtube.com/watch?v=VmQZwfgp3aA)

{:.list-resources}

---
