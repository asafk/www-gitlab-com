---
layout: handbook-page-toc
title: "Security Assurance - ZenGRC Activities"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Purpose

This page provides an overview of the various ZenGRC Activities that are carried out by the [Security Assurance](/handbook/engineering/security/security-assurance/security-assurance.html) sub department. Additionally, this page will also provide information on when stakeholders outside of the Security Assurance sub department may engage with ZenGRC. 

## Field Security Activities
Content is being finalized for this section.

## Risk Acvitities

<details markdown="1">
<summary><a href="https://about.gitlab.com/handbook/engineering/security/security-assurance/risk-field-security/risk-management.html" target="_blank">Security Opeartional Risk Management</a> (StORM)</summary>

All activities related to the StORM program are executed exclusively within ZenGRC. There may be instances where the identification of a risk occurs on GitLab.com (e.g. incident issues, internal issues where security concerns are raised which may be an indicator of risk, etc.) and in these cases, the Risk & Field Security team will review the related details within GitLab and subsequently create a new risk record within ZenGRC for assessment. The wide variety of activities related to StORM that are carried out in ZenGRC include but are not limited to:
* Documenting the identification of a risk
* Documenting the results of risk assessments
* [Scoring](https://about.gitlab.com/handbook/engineering/security/security-assurance/risk-field-security/operational-risk-management-methodology.html#risk-factors-and-risk-scoring) of security operational risks
* Documenting [risk treatment](https://about.gitlab.com/handbook/engineering/security/security-assurance/risk-field-security/operational-risk-management-methodology.html#risk-treatment-options)
* Maintaining the [Risk Register](https://about.gitlab.com/handbook/engineering/security/security-assurance/risk-field-security/risk-management.html#step-3-risk-tracking-and-reporting)
* Task tracking for activities such as execution of the [StORM Annual Risk Assessment](https://about.gitlab.com/handbook/engineering/security/security-assurance/risk-field-security/risk-management.html#storm-procedures)

</details>

<details markdown="1">
<summary><a href="https://about.gitlab.com/handbook/engineering/security/security-assurance/risk-field-security/third-party-risk-management.html" target="_blank">Third Party Risk Management</a></summary>
Content is being finalized for this section.
</details>

## Security Compliance Activities
<details markdown="1">
<summary><a href="/handbook/engineering/security/security-assurance/security-compliance/security-control-lifecycle.html" target="_blank">Continuous Control Monitoring</a></summary>

The GitLab Security Compliance team manages all phases of the [security control lifecycle](/handbook/engineering/security/security-assurance/security-compliance/security-control-lifecycle.html) via ZenGRC using the following objects:
* Programs, Standards, Section, Objectives, and Controls are all used in the Preparation phase
* Assessments and Requests are used in the Testing phase
* Issues are used in the Remediation phase
* Metrics and Reporting are used in the Operating phase

</details>

<details markdown="1">
<summary><a href="/handbook/engineering/security/security-assurance/security-compliance/observation-management.html" target="_blank">Observation Management</a></summary>

Observations (aka: findings, exceptions, issues, deficiencies, Tier 3 operational risks) are recorded and managed within ZenGRC. This allows the Security Compliance team to map those observations out to any and all related systems, control assessments, vendors, etc. as well as capture meaningful data about the current state of our [observation management program](/handbook/engineering/security/security-assurance/security-compliance/observation-management.html) and program operating metrics.
</details>

Project-based work, Quarterly OKR work, and Ad-Hoc workstreams are all captured within ZenGRC tasks assigned to individual members of the Security Compliance team.

## Stakeholder Engagement with ZenGRC

The Security Assurance Team may periodically engage stakeholders that are outside of the sub department using ZenGRC. The various activities that the sub deparment may engage stakeholders on can be found below.

<details markdown="1">
<summary>ZenGRC Questionnaires</summary>

### Completing ZenGRC Questionnaires

Stakeholders may be occasionally engaged to complete a ZenGRC questionnaire. Questionnaires are utilized for various reasons, such as helping to gather and collect data to establish [GitLab's Risk Appetite and Tolerance](https://about.gitlab.com/handbook/engineering/security/security-assurance/risk-field-security/operational-risk-management-methodology.html#risk-appetite-and-tolerance-scoring) year over year. The Security Assurance Team utilizes the native questionnaire functionality within ZenGRC because it provides some mechanisms to automatically calaculate risk scores and thresholds based off of responses. 

Should any team member be engaged to complete a questionnaire from ZenGRC, an example of the email that the team member will receive can be found below.

![Example ZenGRC Questionnaire Email](/handbook/engineering/security/security-assurance/images/zg-questionnaire-example.png)

In order to complete the questionnaire, team members should perform the following steps:

1. Click on the **Open Questionnaire** link to open a web browser window with the questionnaire. 
2. Team members will be presented with the questionnaire. Provide responses to each question until the final question is completed. 

   ![Initial ZenGRC Questionnaire Screen](/handbook/engineering/security/security-assurance/images/example-questionnaire-1.png)

3. Instead of seeing a "submit" button once the final question is answered, team members will need to click on the "summary" button. This screen provides a summary of all of the responses that were provided for the team member to review. The final "submit" button can be found on the summary screen. 

   ![ZenGRC Questionnaire Summary Screen](/handbook/engineering/security/security-assurance/images/example-questionnaire-2.png)

4. Submit the questionnaire. A confirmation screen will be presented.

   ![ZenGRC Submitted Questionnaire Confirmation Screen](/handbook/engineering/security/security-assurance/images/example-questionnaire-3.png)
</details>
